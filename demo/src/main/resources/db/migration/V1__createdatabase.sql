CREATE TABLE IF NOT EXISTS animes (
                                animeid uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
                                name text,
                                description text,
                                type text,
                                yearr integer,
                                image text
                                );
INSERT INTO animes(name, description, type, yearr, image) VALUES
                                                               ('Anime One','This is the One Anime', 'film', 2020,'Anime1.jpg'),
                                                               ('Anime Two','The Two Anime is the next', 'film', 2020, 'Anime2.jpg'),
                                                               ('Anime Three','The Trilogy', 'film', 2020, 'Anime3.jpg'),
                                                               ('Anime Four','Four Animes is too much', 'film', 2020, 'Anime4.jpg');