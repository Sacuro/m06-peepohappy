package com.example.demo.controller;
import com.example.demo.domain.dto.Error;
import com.example.demo.domain.dto.Result;
import com.example.demo.domain.model.File;
import com.example.demo.domain.model.User;
import com.example.demo.repository.FileRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
    @RequestMapping("/files")
public class FileController {

    private final FileRepository fileRepository;
    FileController(FileRepository fileRepository){
        this.fileRepository = fileRepository;
    }
    @GetMapping("/")
    public Result getAllFiles(){
        return new Result(fileRepository.findAll());
    }
    @PostMapping("/")
    public ResponseEntity<?> upload(@RequestParam("file") MultipartFile uploadedFile) {
        try {
            File file = new File();
            file.contenttype = uploadedFile.getContentType();
            file.data = uploadedFile.getBytes();
            fileRepository.save(file);
            return ResponseEntity.ok().body(file);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable UUID id) {
        File file = fileRepository.findById(id).orElse(null);

        if (file == null) return ResponseEntity.notFound().build();

        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(file.contenttype))
                .contentLength(file.data.length)
                .body(file.data);
    }
    @DeleteMapping("/")
    public ResponseEntity<?> deleteAll() {
        fileRepository.deleteAll();
        return ResponseEntity.ok().build();
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable UUID id){
        File file =  fileRepository.findById(id).orElse(null);
        if(file == null){ return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Error.message("No s'ha trobat l'arxiu amb id '"+id+"'"));}
        fileRepository.deleteById(id);
        return  ResponseEntity.status(HttpStatus.ACCEPTED).body(Error.message("S'ha eliminat l'arxiu amb id '"+id+"'"));
    }
}