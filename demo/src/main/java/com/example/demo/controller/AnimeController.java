package com.example.demo.controller;

import com.example.demo.domain.dto.Result;
import com.example.demo.domain.model.Anime;
import com.example.demo.repository.AnimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.domain.dto.Error;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/animes")
public class AnimeController {
    @Autowired
    private final AnimeRepository animeRepository;

    public AnimeController(AnimeRepository animeRepository) {
        this.animeRepository = animeRepository;
    }
    @GetMapping("/")
    public Result findAllAnimes(){
        return new Result(animeRepository.findAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> findAnimeByID(@PathVariable UUID id){
        Anime anime = animeRepository.findById(id).orElse(null);
        if(anime == null){ return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Error.message("No s'ha trobat l'anime amb id '"+id+"'"));}
        return  ResponseEntity.ok().body(anime);
    }
    @PostMapping("/")
    public ResponseEntity<?> createAnime(@RequestBody Anime anime) {
        for (Anime a : animeRepository.findAll()){
            if (String.valueOf(anime.name).matches(a.name)){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Error.message("Ja existeix un anime amb el nom '"+anime.name+"'"));
            }
        }
        animeRepository.save(anime);
        return ResponseEntity.ok().body(anime);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAnimeByID(@PathVariable UUID id){
        Anime anime = animeRepository.findById(id).orElse(null);
        if(anime == null){ return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Error.message("No s'ha trobat l'anime amb id '"+id+"'"));}
        animeRepository.deleteById(id);
        return  ResponseEntity.status(HttpStatus.ACCEPTED).body(Error.message("S'ha eliminat l'anime amb id '"+id+"'"));
    }

}
