/*package ex4;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class a1 {
    public static final Path p = Paths.get("file.json");
    //public List<Contact> contactos;
    public static void main(String[] args) throws IOException {
        File myObj = p.toFile();
        myObj.createNewFile();
        if(args[0]=="add"){
            add(args[1], args[2], args[3]);
        }else if(args[0]=="del"){
            del(args[1]);
        }else if(args[0]=="list"){
            list();
        }else if(args[0]=="search"){
            search(args[1]);
        }else{
            System.out.println("Please specify an action");
        }
    }
    public static void add(String contact_name, String contact_phone, String contact_email) throws IOException {
        Agenda a = new Agenda();
        Contact c = new Contact(contact_name, contact_phone, contact_email);
        //Objetivo: poner el contact en la agenda, Obstucalo: fjaefjfjgirsg
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(p.toFile(), c);
    }
    public static void del(String contact_name){

    }
    public static void search(String contact_email){

    }
    public static void list(){

    }
}
class Contact{
    String contact_name;
    String contact_phone;
    String contact_email;
    public Contact(String contact_name, String contact_phone, String contact_email) {
        this.contact_name = contact_name;
        this.contact_phone = contact_phone;
        this.contact_email = contact_email;
    }
}
class Agenda{
    List<Contact> agenda;
    public Agenda(){

    }
    public Agenda(ArrayList<Contact> agenda) {
        this.agenda = agenda;
    }
}
*/
package main.java.ex4;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class Agenda {
    public List<Contact> ag = new ArrayList<>();
    public Agenda() {}
}

class Contact {
    public String name;
    public String phone;
    public String email;

    public Contact(String name, String phone, String email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public Contact() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}


// ***************************

public class a1 {
    // Escribir ruta y crear objeto para leer el archivo
    public static String ruta = "agenda.json";
    public static Path jsonRuta = Paths.get(ruta);
    public static ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    public static Agenda agendaArchivo;

    public static void main(String[] args) throws IOException {
        // Crear menu para elegir entre add del list search
        leer();
        if(args[0].equals("add")) {
            add(args[1], args[2], args[3]);
        } else if (args[0].equals("del")) {
            del(args[1]);
        } else if (args[0].equals("list")) {
            list();
        } else if (args[0].equals("search")) {
            search(args[1]);
        }else{
            System.out.println("An error ocurred");
        }
    }

    public static void leer() throws IOException {
        try {
            agendaArchivo = objectMapper.readValue(jsonRuta.toFile(), Agenda.class);
        } catch (Exception e) {
            agendaArchivo = new Agenda();
        }
    }

    public static void add(String name, String phone, String email) throws IOException{
        if (agendaArchivo.ag.isEmpty()) {
            agendaArchivo.ag.add(new Contact(name, phone, email));
        }

        for (Contact c : agendaArchivo.ag) {
            if (c.getName().equals(name)) {
                c.setPhone(phone);
                c.setEmail(email);
                break;
            } else {
                agendaArchivo.ag.add(new Contact(name, phone, email));
                break;
            }
        }
        objectMapper.writeValue(jsonRuta.toFile(), agendaArchivo);
    }
    public static void del(String name) throws IOException {
        for (Contact c : agendaArchivo.ag) {
            if ( c.getName().equals(name) ) {
                agendaArchivo.ag.remove(c);
                break;
            }
        }
        objectMapper.writeValue(jsonRuta.toFile(), agendaArchivo);
    }

    public static void list() {
        for (Contact c : agendaArchivo.ag) {
            System.out.println(c.toString());
        }
    }

    public static void search(String email) {
        for (Contact c : agendaArchivo.ag) {
            if (c.getEmail().equals(email)){
                System.out.println(c.toString());
                break;
            }
        }
    }

}