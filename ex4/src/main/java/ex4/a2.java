package main.java.ex4;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class a2 {
    public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    public static String ruta = "library.json";
    public static Path ruta_Json = Paths.get(ruta);
    public static ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    public static Root root;
    public static void main(String[] args) throws IOException, ParseException {
       addbookslibrary();
       addbooksauthor();
       leer();
       if(args[0].equals("0")){

       }else if(args[0].equals("1")){

       }else if(args[0].equals("2")){

       }else if(args[0].equals("3")){

       }else{
           System.out.println("An error ocurred.");
       }
    }
    public static void leer() throws IOException {
        try {
            root = objectMapper.readValue(ruta_Json.toFile(), Root.class);
        } catch (Exception e) {
            root = new Root();
        }
    }
    public static void addbookslibrary(){

    }
    public static void addbooksauthor(){

    }
}
class Root{
    public List<Library> libraries = new ArrayList<>();
    public List<Author> authors = new ArrayList<>();
}
class Library{
    public String name;
    public List<Book> books = new ArrayList<>();
    Library(){}

    public Library(String city, String name) {
        this.name = name;
    }
}
class Book{
    public String title;
    Book(){}

    public Book(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                '}';
    }
}
class Author{
    public String name;
    public List<Book> books = new ArrayList<>();

    public Author(String name) {
        this.name = name;
    }
}

