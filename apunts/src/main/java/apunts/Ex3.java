package apunts;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.nio.file.Files;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Ex3 {

	public static void main(String[] args) throws IOException, Exception {
		Path dir = Paths.get("directori");
		Files.walk(Paths.get("directori")).filter(Files::isRegularFile).forEach(file ->{
			try {
				//System.out.println(file.toString());
				//System.out.println(dir.resolve(file.getFileName()));	
				Files.move(file,dir.resolve(file.getFileName()));
			}catch(IOException e){
				e.printStackTrace();
			}
		});	
		Files.walk(Paths.get("directori")).filter(Files::isDirectory).sorted(Comparator.reverseOrder()).forEach(file ->{
			try {
				Files.delete(file);
			}catch(Exception e) {
				e.printStackTrace();
			}
		});
		Files.walk(Paths.get("directori"))
		.filter(Files::isRegularFile)
		.forEach(file -> {
			try {
				LocalDateTime time = LocalDateTime.parse(Files.getLastModifiedTime(file).toString(), DateTimeFormatter.ISO_DATE_TIME);
				
					Files.createDirectories(Paths.get("directori").resolve(Integer.toString(time.getYear())).resolve(Integer.toString(time.getMonthValue())).resolve(Integer.toString(time.getDayOfMonth()+1)));
				
				Files.move(file, Paths.get("directori").resolve(Integer.toString(time.getYear())).resolve(Integer.toString(time.getMonthValue())).resolve(Integer.toString(time.getDayOfMonth()+1)).resolve(file.getFileName()));
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}