package apunts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class a2 {
    public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    public static String ruta = "library.json";
    public static Path ruta_Json = Paths.get(ruta);
    public static ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    public static Root root;

    public static void main(String[] args) throws IOException, ParseException {
       leer();
       addlibrary();
       addsauthor();
    }
    public static void leer() throws IOException {
        try {
            root = objectMapper.readValue(ruta_Json.toFile(), Root.class);
        } catch (Exception e) {
            root = new Root();
        }
    }
    public static void addlibrary() throws IOException {
        Library l1 = new Library("ciudad1","libreria1");
        Library l2 = new Library("ciudad2", "libreria2");
        Book b1 = new Book("t1");
        Book b2 = new Book("t2");
        Book b3 = new Book("t3");
        Book b4 = new Book("t4");
        Book b5 = new Book("t5");
        Book b6 = new Book("t6");
        l1.books.add(b1);
        l1.books.add(b2);
        l1.books.add(b3);
        l2.books.add(b4);
        l2.books.add(b5);
        l2.books.add(b6);
        root.libraries.add(l1);
        root.libraries.add(l2);
        objectMapper.writeValue(ruta_Json.toFile(), root);
    }
    public static void addsauthor() throws IOException {
        Book b1 = new Book("t1");
        Book b2 = new Book("t2");
        Book b3 = new Book("t3");
        Book b4 = new Book("t4");
        Book b5 = new Book("t5");
        Book b6 = new Book("t6");
        Author a1 = new Author("a1");
        Author a2 = new Author("a2");
        a1.books.add(b1);
        a1.books.add(b2);
        a1.books.add(b3);
        a2.books.add(b4);
        a2.books.add(b5);
        a2.books.add(b6);
        root.authors.add(a1);
        root.authors.add(a2);
        objectMapper.writeValue(ruta_Json.toFile(), root);
    }
}
class Root{
    public List<Library> libraries = new ArrayList<>();
    public List<Author> authors = new ArrayList<>();
}
class Library{
    public String name;
    public List<Book> books = new ArrayList<>();
    Library(){}

    public Library(String city, String name) {
        this.name = name;
    }
}
class Book{
    public String title;
    Book(){}

    public Book(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                '}';
    }
}
class Author{
    public String name;
    public List<Book> books = new ArrayList<>();

    public Author(String name) {
        this.name = name;
    }
}

