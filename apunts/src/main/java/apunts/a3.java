package apunts;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class a3 {
    public static ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    public static void main(String[] args) throws IOException {
        //Api1
        Api1 a = objectMapper.readValue(new URL("https://api.lyrics.ovh/v1/all_time_low/dear_maria_count_me_in"), Api1.class);
        System.out.println(a);
        //Api2
        Api2 a2 = objectMapper.readValue(new URL("https://api.adviceslip.com/advice"), Api2.class);
        System.out.println(a2);
        //Api3
        Api3 a3 = objectMapper.readValue(new URL("https://yomomma-api.herokuapp.com/jokes"), Api3.class);
        System.out.println(a3);
        //Api4
        Api4 a4 = objectMapper.readValue(new URL("https://animechan.vercel.app/api/random"), Api4.class);
        System.out.println(a4);
        //Api5
        Api5 a5 = objectMapper.readValue(new URL("http://api.icndb.com/jokes/random"), Api5.class);
        System.out.println(a5);
        //Api6
        Api6 a6 = objectMapper.readValue(new URL("https://www.boredapi.com/api/activity/"), Api6.class);
        System.out.println(a6);

    }
}
class Api1{
    public String lyrics;
    public Api1() {
    }
    @Override
    public String toString() {
        return "Api1{" +
                "lyrics='" + lyrics + '\'' +
                '}';
    }
}
class Api2{
    public Slip slip;

    @Override
    public String toString() {
        return "Api2{" +
                "slip=" + slip +
                '}';
    }
}
class Slip{
    public int id;
    public String advice;

    @Override
    public String toString() {
        return "Slip{" +
                "id=" + id +
                ", advice='" + advice + '\'' +
                '}';
    }
}

class Api3{
    public String joke;

    @Override
    public String toString() {
        return "Api3{" +
                "joke='" + joke + '\'' +
                '}';
    }
}
class Api4{
    public String anime;
    public String character;
    public String quote;

    @Override
    public String toString() {
        return "Api4{" +
                "anime='" + anime + '\'' +
                ", character='" + character + '\'' +
                ", quote='" + quote + '\'' +
                '}';
    }
}
class Api5{
    public String type;
    public Value value;

    @Override
    public String toString() {
        return "Api5{" +
                "type='" + type + '\'' +
                ", value=" + value +
                '}';
    }
}
class Value{
    public int id;
    public String joke;
    public List<Object> categories;

    @Override
    public String toString() {
        return "Value{" +
                "id=" + id +
                ", joke='" + joke + '\'' +
                ", categories=" + categories +
                '}';
    }
}
class Api6{
    public String activity;
    public String type;
    public int participants;
    public int price;
    public String link;
    public String key;
    public double accessibility;

    @Override
    public String toString() {
        return "Api6{" +
                "activity='" + activity + '\'' +
                ", type='" + type + '\'' +
                ", participants=" + participants +
                ", price=" + price +
                ", link='" + link + '\'' +
                ", key='" + key + '\'' +
                ", accessibility=" + accessibility +
                '}';
    }
}
