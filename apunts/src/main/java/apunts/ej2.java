package apunts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ej2 {
    public static void main(String[]args) throws IOException {
        List<Ordinador> ord = new ArrayList<>();
        ord.add(new Ordinador("234",2,true));
        ord.add(new Ordinador("2131",6,true));
        ord.add(new Ordinador("213",243,false));
        ord.add(new Ordinador("435",289,true));
        ord.add(new Ordinador("54",2432,false));
        metodo1("/tmp/dir/archivo.txt", ord);
        System.out.println(String.valueOf(metodo2("/tmp/dir/archivo.txt")));
    }
    public static void metodo1(String ruta, List<Ordinador> ordinadors) throws IOException {
        if(Files.exists(Paths.get(ruta))){
            Files.delete(Paths.get(ruta));
        }
        for(int i = 0; i<ordinadors.size(); i++){
            //Gerard, como no se si quieres que sobreescriba o no el contenido del archivo,
            // te dejo también los otros sistemas,
            // el primer try comentado solo guarda la última línea,
            // el segundo guarda todo el contenido de la list pero sobreescribe lo que ya estaba en el archivo,
            // y por último el que he dejado sin comentar es el que guarda todo en el archivo y deja sin tocar lo que ya estaba
            //try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(ruta))){
            //try (BufferedWriter bw = new BufferedWriter(new FileWriter(Paths.get(ruta).toFile()))){
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(Paths.get(ruta).toFile(), true))){
                bw.write(String.valueOf(ordinadors.get(i)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public static List<Ordinador> metodo2(String ruta){
        List<Ordinador> ord = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(ruta))){
            for (int a; (a = br.read()) != -1;) {
                String l = br.readLine();
                String[] s = l.split(",");
                ord.add(new Ordinador(s[0], Integer.parseInt(s[1]), Boolean.parseBoolean(s[2])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ord;
    }
}
class Ordinador {
    String numeroSerie;
    int velocitat;
    boolean portatil;
    public Ordinador(String numeroSerie, int velocitat, boolean portatil) {
        this.numeroSerie = numeroSerie;
        this.velocitat = velocitat;
        this.portatil = portatil;
    }

    @Override
    public String toString() {
        return numeroSerie +","+ velocitat +","+ portatil +"\n";
    }
}
