package apunts;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.nio.file.Files;
import java.io.IOException;


public class Ex1 {
	public static void main(String[] args) throws IOException, Exception {
		Path dir = Paths.get("niats");
		Files.walk(Paths.get("niats")).filter(Files::isRegularFile).forEach(file ->{
			try {
				Files.move(file,dir.resolve(file.getFileName()));
			}catch(IOException e){
				e.printStackTrace();
			}
		});	
		Files.walk(Paths.get("niats")).filter(Files::isDirectory).sorted(Comparator.reverseOrder()).forEach(file ->{
			try {
				Files.delete(file);
			}catch(Exception e) {
				e.printStackTrace();
			}
		});
		
	}
}
