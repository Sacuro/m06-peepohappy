package apunts;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static final String filename = "top5.txt";
    static Path p = Paths.get(filename);

    public Main() throws IOException {
    }

    public static void main(String[] args) throws IOException , ClassNotFoundException{
	// write your code here
        String name = args[0];
        int points = Integer.parseInt(args[1]);
        System.out.println("** TOP 5 SCORE **");
        createFile();
        printtop5(name, points);
    }
    public static void createFile() throws IOException {
        try {
            File myObj = new File(filename);
            myObj.createNewFile();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    public static void printtop5(String name, int points)throws IOException, ClassNotFoundException{
        //Gets content into a list, then uses for-each to print content
        TopFive t5;
        try(ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(p))){
            t5 = (TopFive) ois.readObject();
        } catch (IOException e){
            // There are no scores
            t5 = new TopFive();
        }
        t5.scores.add(new Points(name, points));
        t5.scores = t5.scores.stream()
                .sorted(Comparator.comparing(Points::getScore).reversed())
                .limit(5)
                .collect(Collectors.toList());
        ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(p));
        oos.writeObject(t5);
        System.out.println(t5);
    }


}
class Points implements Serializable{
    public String name;
    public int score;

    public Points(String name, int score){
        this.name = name;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return name + '\t' +
                + score;
    }
}
class TopFive implements Serializable{
    public List<Points> scores = new ArrayList<>();

    @Override
    public String toString() {
        return  scores.stream()
                .map(Object::toString)
                .collect(Collectors.joining("\n"));
    }
}
