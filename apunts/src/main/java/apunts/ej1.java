package apunts;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class ej1 {
    public static void main(String[]args) throws IOException{
        Path dir = Paths.get(args[0]);
        Files.walk(dir).filter(Files::isRegularFile).forEach(file ->{
            try{
                FileTime f = Files.getLastModifiedTime(file);
                Instant i = f.toInstant();
                if(i.isBefore(Instant.now().minus(Integer.parseInt(args[1]), ChronoUnit.DAYS))){
                    Files.delete(file);
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        });
    }
}
