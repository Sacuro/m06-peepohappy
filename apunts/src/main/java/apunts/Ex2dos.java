package apunts;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Ex2dos {

	public static void main(String[] args) throws IOException{
		Scanner sc = new Scanner(System.in);
		System.out.println("Dime la ruta del archivo");
		String ruta  = sc.nextLine();
		System.out.println("Dime el numero de trozos a hacer del archivo");
		int trozos  = sc.nextInt();
		separador(ruta,trozos);
		juntador(ruta);
		sc.close();
	}
	public static void separador(String ruta, int trozos) throws IOException{
		Path file = Paths.get(ruta);
		int partCounter = 1;
		InputStream is = Files.newInputStream(file);
		Path outfile = Paths.get(file.getFileName() + ".part" + partCounter);

		OutputStream os = Files.newOutputStream(outfile);

		int sizeOfFiles = ((int)Math.ceil(is.available()/trozos));// 1MB

		int amountBytes = 0;
		int byteLeido;
		while ((byteLeido = is.read()) != -1) {
			
			if (amountBytes == sizeOfFiles && ((int) Math.ceil(is.available() / trozos)) != 0) {
				partCounter++;
				outfile = Paths.get(file.getFileName() + ".part" + partCounter);
				os = Files.newOutputStream(outfile);
				amountBytes = 0;
			}
			amountBytes++;
			os.write(byteLeido);
		}
	}
	public static void juntador(String ruta) throws IOException {
		Path p = Paths.get(ruta);
		OutputStream os = Files.newOutputStream(p);
		for (int i=1; ;i++) {
			String part = ruta + ".part" + i;
			File tmp = new File(part);
			if (tmp.exists()) {
				Path p2 = Paths.get(part);
				os.write(Files.readAllBytes(p2));
			} else {
				break;
			}
		}
	}
}
	

