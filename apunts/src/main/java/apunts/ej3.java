package apunts;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ej3 {
    public static void main(String[]args) throws IOException {
        bytes(args);
        //Este ultimo metodo no va, dentro de él lo explico en detalle
        //carac(args);
    }
    public static void bytes(String[]args) throws IOException {
        Files.walk(Paths.get(args[1])).filter(Files::isRegularFile).forEach(file ->{
            if(!file.endsWith(args[2])){
                try {
                    if(Files.exists(Paths.get("/tmp/dir/archivotemporal.txt"))){
                        Files.delete(Paths.get("/tmp/dir/archivotemporal.txt"));
                    }
                    Files.createFile(Paths.get("/tmp/dir/archivotemporal.txt"));
                    OutputStream os = new FileOutputStream(Paths.get("/tmp/dir/archivotemporal.txt").toFile(), true);
                    InputStream is = Files.newInputStream(Paths.get(args[0]));
                    InputStream is2 = Files.newInputStream(Paths.get(file.toString()));
                    os.write(is.readAllBytes());
                    os.write(is2.readAllBytes());
                    File f = Paths.get("/tmp/dir/archivotemporal.txt").toFile();
                    f.renameTo(Paths.get(String.valueOf(file)).toFile());
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        });
    }

    public static void carac(String[]args) throws IOException {
        Files.walk(Paths.get(args[1])).filter(Files::isRegularFile).forEach(file ->{
            if(!file.endsWith(args[2])){
                try {
                    if(Files.exists(Paths.get("/tmp/dir/archivotemporal.txt"))){
                        Files.delete(Paths.get("/tmp/dir/archivotemporal.txt"));
                    }
                    Files.createFile(Paths.get("/tmp/dir/archivotemporal.txt"));
                    BufferedWriter bf = new BufferedWriter(new FileWriter(Paths.get(args[0]).toFile(), true));

                    BufferedReader br = new BufferedReader(new FileReader(Paths.get(args[0]).toFile()));
                    BufferedReader br2 = new BufferedReader(new FileReader(Paths.get(file.toString()).toFile()));

                    StringBuilder everything = new StringBuilder();
                    String line;
                    while( (line = br.readLine()) != null) {
                        everything.append(line);
                    }
                    while( (line = br2.readLine()) != null) {
                        everything.append(line);
                    }
                    // Hasta aqui todo funciona bien, pero al llegar al write por algun motivo no escribe nada :/
                    bf.write(everything.toString());
                    File f = Paths.get("/tmp/dir/archivotemporal.txt").toFile();
                    f.renameTo(Paths.get(String.valueOf(file)).toFile());
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        });
    }
}
