
package apunts;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class Agenda {
    public List<Contact> ag = new ArrayList<>();
    public Agenda() {}
}

class Contact {
    public String name;
    public String phone;
    public String email;

    public Contact(String name, String phone, String email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public Contact() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

public class a1 {
    public static String ruta = "agenda.json";
    public static Path jsonRuta = Paths.get(ruta);
    public static ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    public static Agenda agenda;

    public static void main(String[] args) throws IOException {
        leer();
        if(args[0].equals("add")) {
            add(args[1], args[2], args[3]);
        } else if (args[0].equals("del")) {
            del(args[1]);
        } else if (args[0].equals("list")) {
            list();
        } else if (args[0].equals("search")) {
            search(args[1]);
        }else{
            System.out.println("An error ocurred");
        }
    }

    public static void leer() throws IOException {
        try {
            agenda = objectMapper.readValue(jsonRuta.toFile(), Agenda.class);
        } catch (Exception e) {
            agenda = new Agenda();
        }
    }

    public static void add(String name, String phone, String email) throws IOException{
        if (agenda.ag.isEmpty()) {
            agenda.ag.add(new Contact(name, phone, email));
        }

        for (Contact c : agenda.ag) {
            if (c.getName().equals(name)) {
                c.setPhone(phone);
                c.setEmail(email);
                break;
            } else {
                agenda.ag.add(new Contact(name, phone, email));
                break;
            }
        }
        objectMapper.writeValue(jsonRuta.toFile(), agenda);
    }
    public static void del(String name) throws IOException {
        for (Contact c : agenda.ag) {
            if ( c.getName().equals(name) ) {
                agenda.ag.remove(c);
                break;
            }
        }
        objectMapper.writeValue(jsonRuta.toFile(), agenda);
    }

    public static void list() {
        for (Contact c : agenda.ag) {
            System.out.println(c.toString());
        }
    }
    public static void search(String email) {
        for (Contact c : agenda.ag) {
            if (c.getEmail().equals(email)){
                System.out.println(c.toString());
                break;
            }
        }
    }

}