package apunts;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Ex2 {
	static boolean fileFind;
	public static void main(String[] args) throws IOException {
		Path dirA = Paths.get("dirA");
		Path dirB = Paths.get("dirB");
		System.out.println("Dir A:"+"\n");
		comparator(dirA, dirB);
		System.out.println("Dir B:");
		comparator(dirB, dirA);
	}
	
	public static void comparator(Path dir1, Path dir2) throws IOException {				
		Files.walk(dir1)
		.filter(Files::isRegularFile)
		.forEach(file -> {
			try {
				byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
				fileFind = false;
				
				Files.walk(dir2)
				.filter(Files::isRegularFile)
				.forEach(file2 -> {
					try {
						byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
						boolean fileName = false, fileHash = false;
						if(dir1.resolve(file.getFileName()).equals(dir2.resolve(file2.getFileName()))) {
							fileName = true;
						} 
						if(Arrays.equals(hash1, hash2)) {
							fileHash = true;
						}
						
						if(fileName && fileHash) {
							fileFind = true;
							System.out.println("The file " + file.getFileName() + " it's in the same route ");
						} else if (!fileName && fileHash) {
							fileFind = true;
							System.out.println("The file " + file.getFileName() + " is renamed as: " + dir2.resolve(file2.getFileName()));
						} 
					}catch(IOException e) {
						e.printStackTrace();
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					}
				});
				if(!fileFind){
					System.out.println("The file " + file.getFileName() + " is missing");
				}
			}catch(IOException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
			}
		});
	}
}
