package com.example.demo.controller;

import com.example.demo.domain.dto.Error;
import com.example.demo.domain.dto.ResponseList;
import com.example.demo.domain.model.projection.ProjectionGenre;
import com.example.demo.repository.GenresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/genres")
public class GenreController {
    @Autowired
    private final GenresRepository genresRepository;

    public GenreController(GenresRepository genresRepository) {
        this.genresRepository = genresRepository;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAllGenres(){
        return ResponseEntity.ok().body(new ResponseList(genresRepository.findBy(ProjectionGenre.class)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findGenderByID(@PathVariable UUID id){
        List<ProjectionGenre> genre = genresRepository.findByGenreid(id, ProjectionGenre.class);
        if (genre != null) {
            return  ResponseEntity.ok().body(genre);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Error.message("No s'ha trobat el genre amb id '" + id + "'"));

    }

}