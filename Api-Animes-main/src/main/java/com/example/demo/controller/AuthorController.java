package com.example.demo.controller;

import com.example.demo.domain.dto.Error;
import com.example.demo.domain.dto.ResponseList;
import com.example.demo.domain.model.projection.ProjectionAuthor;
import com.example.demo.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/authors")
public class AuthorController {
    @Autowired
    private final AuthorRepository authorsRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorsRepository = authorRepository;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAllAuthors(){
        return ResponseEntity.ok().body(new ResponseList(authorsRepository.findBy(ProjectionAuthor.class)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findAuthorByID(@PathVariable UUID id){
        List<ProjectionAuthor> author = authorsRepository.findByAuthorid(id, ProjectionAuthor.class);
        if (author != null) {
            return  ResponseEntity.ok().body(author);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Error.message("No s'ha trobat l'autor amb id '" + id + "'"));

    }

}