package com.example.demo.controller;

import com.example.demo.domain.dto.Error;
import com.example.demo.domain.dto.Result;
import com.example.demo.domain.dto.UserRegisterRequest;
import com.example.demo.domain.model.Anime;
import com.example.demo.domain.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired private UserRepository userRepository;
    @Autowired private BCryptPasswordEncoder passwordEncoder;
    @GetMapping("/")
    public Result getAllUsers(){
        return new Result(userRepository.findAll());
    }
    @PostMapping("/")
    public ResponseEntity<?> register(@RequestBody UserRegisterRequest userRegisterRequest) {

            if (userRepository.findByUsername(userRegisterRequest.username) == null) {
            User user = new User();
            user.username = userRegisterRequest.username;
            user.password = passwordEncoder.encode(userRegisterRequest.password);
            userRepository.save(user);
            return ResponseEntity.ok().body(user);   // TODO
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Error.message("Ja existeix un usuari amb el nom "+ userRegisterRequest.username+"'")); // TODO
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable UUID id){
        User user = (User) userRepository.findById(id).orElse(null);
        if(user == null){ return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Error.message("No s'ha trobat l'user amb id '"+id+"'"));}
        userRepository.deleteById(id);
        return  ResponseEntity.status(HttpStatus.ACCEPTED).body(Error.message("S'ha eliminat l'user amb id '"+id+"'"));
    }
    @DeleteMapping("/")
    public ResponseEntity<?> deleteAllUsers(){
        userRepository.deleteAll();
        return ResponseEntity.ok().build();
    }
    @GetMapping("/favorites")
    public Result getUserFavs(@AuthenticationPrincipal UserDetails user){
        return new Result(Arrays.asList(userRepository.findByUsername(user.getUsername()).favorites.toArray()));
    }
}