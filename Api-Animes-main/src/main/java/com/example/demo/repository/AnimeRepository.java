package com.example.demo.repository;


import com.example.demo.domain.model.Anime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AnimeRepository extends JpaRepository<Anime, UUID> {
    void deleteById(UUID id);
    Anime findByName(String nombre);
    <T> List<T> findByAnimeid(UUID animeid, Class<T> type);
    <T> List<T> findBy(Class<T> type);
}
