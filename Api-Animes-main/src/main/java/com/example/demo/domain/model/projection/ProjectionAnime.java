package com.example.demo.domain.model.projection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Set;
import java.util.UUID;

@JsonPropertyOrder({"animeid","name","description","type","yearr","image","authors","genres"})
public interface ProjectionAnime {

    UUID getAnimeid();
    String getName();
    String getDescription();
    String getType();
    int getYearr();
    String getImage();

    @JsonIgnoreProperties("animes")
    Set<ProjectionAuthor> getAuthors();

    @JsonIgnoreProperties("genres")
    Set<ProjectionGenre> getGenres();
}
