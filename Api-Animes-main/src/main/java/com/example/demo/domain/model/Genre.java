package com.example.demo.domain.model;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "genres")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public UUID genreid;

    public String label;
    public String image;

    @ManyToMany(mappedBy = "genres")
    public Set<Anime> animes;

    public Set<Anime> getAnimes() {
        return animes;
    }

    public void setAnimes( Set<Anime> animes ) {
        this.animes = animes;
    }

    public UUID getGenreid() {
        return genreid;
    }

    public void setGenreid( UUID genreid ) {
        this.genreid = genreid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel( String label ) {
        this.label = label;
    }

    public String getImage() {
        return image;
    }

    public void setImage( String image ) {
        this.image = image;
    }
}
