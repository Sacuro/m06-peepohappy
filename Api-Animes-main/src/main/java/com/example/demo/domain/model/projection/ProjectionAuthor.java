package com.example.demo.domain.model.projection;

import java.util.UUID;

public interface ProjectionAuthor {
    UUID getAuthorid();
    String getName();
    String getImage();
}
