package com.example.demo.domain.model.projection;

import java.util.UUID;

public interface ProjectionGenre {
    UUID getGenreid();
    String getLabel();
    String getImage();
}
