package com.musiquitaapp.Yt;

import static com.musiquitaapp.Yt.Auth.SCOPES;

import android.accounts.Account;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;

import com.google.android.gms.common.AccountPicker;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.musiquitaapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class YoutubeSingleton {

    private static YouTube youTube;
    private static YouTube youTubeWithCredentials;
    private static GoogleAccountCredential credential;

    /*
    private static YouTube youTube;
    private static Credential credential;*/
    private static final String CLIENT_SECRETS= "/client_secret.json";
    private static final Collection<String> SCOPES =
            Arrays.asList(YouTubeScopes.YOUTUBE, YouTubeScopes.YOUTUBE_READONLY);
    private static final GsonFactory GSON_FACTORY = GsonFactory.getDefaultInstance();



    private static YoutubeSingleton ourInstance = new YoutubeSingleton();

    public static YoutubeSingleton getInstance() {
        return ourInstance;
    }

    private YoutubeSingleton() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        try {
            credential = GoogleAccountCredential.usingOAuth2(YTApplication.getAppContext(), SCOPES).setBackOff(new ExponentialBackOff());
            final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            youTube = new YouTube.Builder(httpTransport, GSON_FACTORY, new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest httpRequest) throws IOException {

                }
            }).setApplicationName(YTApplication.getAppContext().getString(R.string.app_name))
                    .build();
            credential.setSelectedAccount(new Account("joanmcoria@gmail.com", "com.musiquitaapp"));
            youTubeWithCredentials = new YouTube.Builder(new NetHttpTransport(),GSON_FACTORY , credential)
                    .setApplicationName(YTApplication.getAppContext().getString(R.string.app_name))
                    .build();

        }catch (IOException | GeneralSecurityException e){
            e.printStackTrace();
        }
    }
        /*
        try {

            final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            InputStream in = YoutubeSingleton.class.getResourceAsStream(CLIENT_SECRETS);
            GoogleClientSecrets clientSecrets =
                    GoogleClientSecrets.load(GSON_FACTORY, new InputStreamReader(in));
            // Build flow and trigger user authorization request.
            GoogleAuthorizationCodeFlow flow =
                    new GoogleAuthorizationCodeFlow.Builder(httpTransport, GSON_FACTORY, clientSecrets, SCOPES)
                            .build();
            credential = GoogleAccountCredential.usingOAuth2(YTApplication.getAppContext(), SCOPES);
                    new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
            youTube = new YouTube.Builder(httpTransport, GSON_FACTORY, credential)
                    .setApplicationName(YTApplication.getAppContext().getString(R.string.app_name))
                    .build();

        }catch (IOException | GeneralSecurityException e){
            e.printStackTrace();
        }
    }
*/
    public static YouTube getYouTube() {
        return youTube;
    }

    public static YouTube getYouTubeWithCredentials() {
        return youTubeWithCredentials;
    }

    public static GoogleAccountCredential getCredential() {
        return credential;
    }

    /*
    public static YouTube getYouTubeWithCredentials() {
        return youTube;
    }

    public static Credential getCredential() {
        return credential;
    }*/
}