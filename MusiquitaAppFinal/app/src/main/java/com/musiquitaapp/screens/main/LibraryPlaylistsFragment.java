package com.musiquitaapp.screens.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.musiquitaapp.R;
import com.musiquitaapp.Yt.YoutubeSingleton;

import java.io.IOException;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LibraryPlaylistsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LibraryPlaylistsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LibraryPlaylistsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LibraryPlaylistsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LibraryPlaylistsFragment newInstance(String param1, String param2) {
        LibraryPlaylistsFragment fragment = new LibraryPlaylistsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        YouTube yt = YoutubeSingleton.getYouTubeWithCredentials();
        Thread CrearEventoHilo = new Thread(){
            public void run(){

                try {
                    PlaylistListResponse request = yt.playlists().list(Collections.singletonList("snippet")).setMine(true).execute();
                    System.out.println(request);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
        CrearEventoHilo.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_library_playlists, container, false);
    }
}