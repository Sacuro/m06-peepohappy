package com.musiquitaapp.screens.main;

import static com.musiquitaapp.Yt.YoutubeSingleton.getCredential;
import static com.musiquitaapp.Yt.YoutubeSingleton.getYouTubeWithCredentials;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.musiquitaapp.R;
import com.musiquitaapp.databinding.FragmentCreateAccountBinding;
import com.musiquitaapp.databinding.FragmentHomeBinding;
import com.musiquitaapp.screens.media.PlayerActivity;

import java.io.IOException;
import java.util.Collections;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.followerArtistText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Intent TEMPORAL!!! de dashboard a reproductor
                Intent intent = new Intent(getActivity(), PlayerActivity.class);
                startActivity(intent);
            }
        });

        // TODO HomeFragment cosas

        return view;
    }
}