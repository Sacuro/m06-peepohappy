package com.musiquitaapp.screens.login;

//Imports talibanes
import static android.app.Activity.RESULT_OK;
import static com.musiquitaapp.Yt.YoutubeSingleton.getCredential;
import static com.musiquitaapp.Yt.YoutubeSingleton.getYouTubeWithCredentials;
import com.google.android.gms.auth.GoogleAuthUtil;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.android.gms.common.AccountPicker;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.PlaylistItemListResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
//Fin de imports talibanes


import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.musiquitaapp.R;
import com.musiquitaapp.databinding.FragmentRegisterSelectorBinding;
import com.musiquitaapp.screens.BaseFragment;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class RegisterSelector extends BaseFragment {
    //Codigo taliban//
    private static final int PERMISSIONS = 1;
    static final int REQUEST_ACCOUNT_PICKER = 2;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    public static final String PREF_ACCOUNT_NAME = "accountName";
    //
    private FragmentRegisterSelectorBinding binding;

    public RegisterSelector() {
        // Required empty public constructor
    }
    private static final GsonFactory GSON_FACTORY = GsonFactory.getDefaultInstance();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRegisterSelectorBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.buttonEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_registerSelector_to_createAccount);
            }
        });

        binding.buttonGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO login con google

                //navController.navigate(R.id.action_registerSelector_to_dashboardActivity);
                //Bomba iraní
                requestPermissions();

            }
        });

        binding.buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_registerSelector_to_login);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    //Codigo robado de talibanes
    @AfterPermissionGranted(PERMISSIONS)
    private void requestPermissions() {


        if (EasyPermissions.hasPermissions(this.getContext(), Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_PHONE_STATE)) {
            // Already have permission, do the thing

            if (EasyPermissions.hasPermissions(this.getContext(), Manifest.permission.GET_ACCOUNTS)) {
                String accountName =  getActivity().getPreferences(Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, null);
                if (accountName != null) {
                    getCredential().setSelectedAccountName(accountName);



                    navController.navigate(R.id.action_registerSelector_to_dashboardActivity);
                } else {
                    // Start a dialog from which the user can choose an account

                    startActivityForResult(getCredential().newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);

                    //navController.navigate(R.id.action_registerSelector_to_dashboardActivity);


                }
            } else {
                // Request the GET_ACCOUNTS permission via a user dialog
                EasyPermissions.requestPermissions(
                        this,
                        "This app needs to access your Google account (via Contacts).",
                        REQUEST_PERMISSION_GET_ACCOUNTS,
                        Manifest.permission.GET_ACCOUNTS);
            }
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.all_permissions_request),
                    PERMISSIONS, Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_PHONE_STATE);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ACCOUNT_PICKER) {
            if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
                String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                if (accountName != null) {
                    SharedPreferences settings = getActivity().getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(PREF_ACCOUNT_NAME, accountName);
                    editor.apply();
                    navController.navigate(R.id.action_registerSelector_to_dashboardActivity);

                }
            }
        }
    }
    /*
    public static Credential authorize(final NetHttpTransport httpTransport) throws IOException {

        // Load client secrets.
        InputStream in = RegisterSelector.class.getResourceAsStream(CLIENT_SECRETS);
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(GSON_FACTORY, new InputStreamReader(in));
        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(httpTransport, GSON_FACTORY, clientSecrets, SCOPES)
                        .build();
        Credential credential =
                new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        return credential;
    }
     */
    //Codigo robado de talibanes
}