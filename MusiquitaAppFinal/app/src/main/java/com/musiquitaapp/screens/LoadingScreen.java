package com.musiquitaapp.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.musiquitaapp.R;

public class LoadingScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);
    }
}