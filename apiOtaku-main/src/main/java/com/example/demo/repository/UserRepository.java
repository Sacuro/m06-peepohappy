package com.example.demo.repository;

import com.example.demo.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    User findByUsername(String username);
    Optional<User> findById(UUID id);

    void deleteById(UUID id);


    <T> List<T> findByUsername(String username, Class<T> type);
}
