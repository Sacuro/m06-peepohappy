package com.example.apiOtaku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiOtakuApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiOtakuApplication.class, args);
	}

}
